import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, Nav} from 'ionic-angular';
import {FeedProvider, Feed} from '../../providers/feed/feed';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {
    @ViewChild(Nav) nav: Nav;

    rootPage = 'FeedListPage';
    feeds: Feed[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private feedProvider: FeedProvider,
        public alertCtrl: AlertController,
        public storage: Storage
    ) {

    }

    public addFeed() {
        
        let prompt = this.alertCtrl.create({
            title: 'Add URL Feed',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Nome do feed'
                },
                {
                    name: 'url',
                    placeholder: 'http://www.myfeedurl.com/feed'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        
                        if(!data.name) {
                            
                            this.alerta('Erro!', 'Digite o Título da lista de feeds!');
                            return;
                        }
                        if(!data.url) {
                            
                            this.alerta('Erro!', 'Digite a URL dos feeds!');
                            return;
                        }
                        
                        let newFeed = new Feed(data.name, data.url);
                        this.feedProvider.addFeed(newFeed).then(
                            res => {
                                this.loadFeeds();
                            }
                        );
                    }
                }
            ]
        });
        prompt.present();
    }

    private loadFeeds() {
        this.feedProvider.getSavedFeeds().then(
            allFeeds => {
                this.feeds = allFeeds;
            });
    }

    public openFeed(feed: Feed) {
        this.navCtrl.setRoot('FeedListPage', {'selectedFeed': feed});
    }

    public deleteFeed(feed: Feed) {

        let confirm = this.alertCtrl.create({
            title: 'Excluir Registro',
            message: 'Deseja excluir este Feed de notícias?',
            buttons: [
                {
                    text: 'Não',
                    handler: () => {

                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        this.feedProvider.delFeed(feed).then(
                            res => {
                                this.loadFeeds();
                            }
                        );
                    }
                }
            ]
        });
        confirm.present();

    }

    public ionViewWillEnter() {
        this.loadFeeds();
    }
    
    alerta(title, msg) {
        
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['Ok']
        });
        alert.present();
    }

}
