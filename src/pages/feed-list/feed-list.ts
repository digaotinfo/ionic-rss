import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

import {InAppBrowser} from '@ionic-native/in-app-browser';
import {FeedProvider, FeedItem, Feed} from '../../providers/feed/feed';
import {HomePage} from '../../pages/home/home';

/**
 * Generated class for the FeedListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-feed-list',
    templateUrl: 'feed-list.html',
})
export class FeedListPage {

    articles: FeedItem[];
    selectedFeed: Feed;
    loading: Boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private iab: InAppBrowser,
        private feedProvider: FeedProvider,
        private socialSharing: SocialSharing
    ) {

        this.selectedFeed = navParams.get('selectedFeed');
        this.carregaFeeds();
    }

    public openArticle(url: string) {
        this.iab.create(url, '_blank');
    }

    loadArticles() {
        
        this.loading = true;
        this.feedProvider.getArticlesForUrl(this.selectedFeed.url).subscribe(res => {
            this.articles = res;
            this.loading = false;
        });
    }

    public carregaFeeds() {
        
        if (this.selectedFeed !== undefined && this.selectedFeed !== null) {
            this.loadArticles()
        } else {
            this.feedProvider.getSavedFeeds().then(
                feeds => {
                    if (feeds.length > 0) {
                        let item = feeds[0];
                        this.selectedFeed = new Feed(item.title, item.url);
                        this.loadArticles();
                    }
                }
            );
        }
    }
    
    comp(feed){
        
        let compartilhamentoTxt = feed.title + " (" + feed.link + ")";
        this.socialSharing.share(compartilhamentoTxt, null, null, null);
    }
    
    voltarHome() {
        this.navCtrl.setRoot('HomePage');
    }

}
